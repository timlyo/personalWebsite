FROM abiosoft/caddy

RUN apk update

COPY CaddyFile /etc/Caddyfile
COPY output /srv/http

EXPOSE 2015/tcp