_:
    @just -l

run_server: build
    caddy --conf devCaddyFile

build:
    pipenv run python builder


watch_files:
    watchexec -c -w src -- pipenv run python builder --dev true

upload:
    docker build . -t personal-website
    docker save -o personal-website.tar personal-website
    zip personal-website.zip personal-website.tar
    rsync personal-website.zip timmaddison.co.uk:/tmp/personal-website.zip --progress

    ssh -t timmaddison.co.uk unzip -o /tmp/personal-website.zip
    ssh -t timmaddison.co.uk docker stop personal-website || true
    ssh -t timmaddison.co.uk docker rename personal-website personal-website-old || true
    ssh -t timmaddison.co.uk docker load -i personal-website.tar
    ssh -t timmaddison.co.uk docker run -p2015:2015 -d --name personal-website personal-website

