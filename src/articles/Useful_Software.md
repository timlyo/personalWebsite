---
date: 2016/05/17
outline: Collection of useful software that I want to remember
public: True
---

# A collection of software that I've found exceptionally useful over the years

This is a collection of some of the lesser known software that has been useful. This means that Vim wont be on this list, despite it being opened on a daily basis. This is mostly software that I find useful but am likely to forget the name of.

## nethogs

Simple table of bandwidth usage by process

## htop

System and process information

## Caddy

A modern, secure by default web server.

## Just

A command runner. Inspired by Make, but not designed for just building software.

## WatchExec

A Commandline runner. Watches for file changes and then executes a command.
