---
date: 2015/05/08
outline: A collection of resources that I've found useful
public: true
---

## Useful Links


A collection of links that I've found useful over the years

### Linux

* [Bash Cheat sheet](http://cli.learncodethehardway.org/bash_cheat_sheet.pdf)
* [Git Cheat Sheet](https://training.github.com/kit/downloads/github-git-cheat-sheet.pdf)
* [Reading system info with C](http://www.makerdyne.com/blog/reading-linux-system-info-with-c/)
* [Modern Rosetta Stone](https://certsimple.com/rosetta-stone)

### Programming

* [Programatically creating an icosphere mesh](http://blog.andreaskahler.com/2009/06/creating-icosphere-mesh-in-code.html)
* [OpenGL red book](http://www.glprogramming.com/red/)
* [Error handling in rust](http://blog.burntsushi.net/rust-error-handling/)
* [Visual machine learning](http://www.r2d3.us/visual-intro-to-machine-learning-part-1/)
* [Neural networks and deep learning](http://neuralnetworksanddeeplearning.com/)
* [Backpropogation](https://colah.github.io/posts/2015-08-Backprop/)

### Other stuff

* [Programming sucks](http://www.stilldrinking.org/programming-sucks)
* [Gitxiv: Collaborative open Computer Science](http://gitxiv.com/)


### Maths

* [Interactive guide to the fourier transform](http://betterexplained.com/articles/an-interactive-guide-to-the-fourier-transform/)

### Humour

* [If PHP were British](https://www.addedbytes.com/blog/if-php-were-british/)
