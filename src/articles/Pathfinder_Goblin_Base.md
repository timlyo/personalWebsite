---
date: 2018/09/18
outline: Pathfinder gamemode based around building a goblin base
---

# Goblin Base

Goblins are angry, rude, illiterate, and hateful creatures that spend their lives living in squalor, stealing scraps from anyone who dares to come too close. The similarity to the average player makes this game an obvious choice. 

Goblin Base is a campaign based around a tribe of goblins building up a lair and defending it from attackers.

## The Goblin

Goblins are small CR 1/3 creatures, who live short angry lives. They have keen dark-vision and prefer to live in caves. They are very superstitious and treat magic with both awe and fear. The players play stock goblins in this mode, simply using the monster page. They can optionally take one of the variants (CR+ at dm's discretion) or any of the alternative racial traits.


## Mechanics

### Time

Goblin Base is designed for a long term base, therefore time is of the essence. Time is based around week long sessions of building followed by an encounter. Time is then further broken down into 21 1/3 days. Some actions take a single third of a day e.g. sleeping, crafting simple items. Other tasks take multiple such as hunting, and some are entirely dependant on skill such as mining.

### Initial Base Creation

It may be desirable for players to start with an already constructed base. This is done by giving the players the following bases, allowing them to place them down on the game board and drawing around the exterior:

* 1d4 of huge bases (3x3)
* 1d6 of large bases (2x2)
* 1d8 of medium bases (1x1)

This gives the players an average base size of 2.5 × 9 + 3.5 × 4 + 4.5 × 1 = 41 tiles and encourages the formation of small connected caverns. These may be laid out in any way that the players see fit and entrances may be made anywhere the players wish.

### Mining

Mining is inspired by Races of the Dragon<sup>1</sup> but has been adapted to account for longer term play and the, ahem, inadequacies of the average goblin. A single mining action removes a 5ft square and is initialised by a str check. If the player is a Miner or a Sapper (or other related professions) then this can optionally be an int check.

| Material       | DC  |
| --:            | :-- |
| Gravel or Sand | 5   |
| Dirt           | 10  |
| Clay or Silt   | 15  |
| Soft Rock      | 20  |
| Hard Rock      | 25  |

If the player isn't trained then add 5 to the check. Up to 2 other goblins can aid another with digging, each assistant who rolls 10 or higher grants a +2 bonus to the lead miner.

If the check is passed then the player can mine a single block out in 1 day. Every 5 that the check is exceeded by reduces the digging time by 1/3 of a day.

### Traps

Crating traps is inspired by the alternate crafting rules <sup>2</sup>. Crafting can be done without the Craft(traps) skill, but is done at a -5. Failing the check by 10 or more triggers the trap on the builder. The cost of the trap is expressed in raw resources, imagination may be required.

| CR    | Craft DC |
| --:   | :--      |
| 1-5   | 20       |
| 6-10  | 25       |
| 11-15 | 30       |
| 16+   | 35       |

Note that traps can add additional [features](https://www.d20pfsrd.com/skills/alternate-crafting-rules/), but these increase the CR and therefore the time.

Crafting a trap takes 2 × CR days. The time is reduced by 1/3 of a day for every 5 that the DC is exceeded by.

### Food

Feeding a goblin is vital to its survival, and generally to the survival of those around it. Food is handled on group basis with a single food score, with every goblin eating 1 food from it each day.

The main way that goblins get food is by tearing apart anything that gets too close to their cave. There are a number of more civilised methods though. The most obvious way is by foraging. This takes 1/3 of a day at a DC of 10, every 2 points over 10 results in 1 extra food.

The alternative is hunting. This is more dangerous, but may also result in more food. Hunting takes 1/3 of a day. The creature encountered is determined by an [encounter roll
](https://www.d20pfsrd.com/bestiary/indexes-and-tables/encounter-tables/). It is up to the dm whether to half the d100 result to make the encounter easier and more likely to result in food.

| Size Category | Food |
| --:           | :--  |
| Small         | 5    |
| Medium        | 25   |
| Large         | 50   |

## To be determined

* Wood cutting rules
* List of traps
* Professions
* Classes
* Hunting specific encounter table
* Alternative method of creating new goblins

## Sources

1. [Races of the Dragon](http://choisey.free.fr/3.5/Help/Races%20of%20the%20Dragon%20(OEF).pdf)
2. [Alternate Crafting Rules](https://www.d20pfsrd.com/skills/alternate-crafting-rules/)

## Useful Links

* [Goblin Monster](https://www.d20pfsrd.com/bestiary/monster-listings/humanoids/goblin/)
* [Goblin Race](https://www.d20pfsrd.com/races/other-races/featured-races/arg-goblin/)
* [We be Goblins](https://rpg.rem.uz/Pathfinder/Modules/We%20Be%20Goblins%21.pdf)