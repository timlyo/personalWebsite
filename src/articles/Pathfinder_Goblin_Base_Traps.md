---
date: 2018/09/30
outline: Traps for goblin base
---

The following contains content from [d20pfsrd.com](https://www.d20pfsrd.com/) which is under the Open Game License.

# Goblin Base Traps

### [Punji Stake Trap](https://www.d20pfsrd.com/gamemastering/traps-hazards-and-special-terrains/traps/punji-stake-trap) CR 1/2

Type mechanical; Perception DC 20; Disable Device DC 20

Trigger location; Reset manual

This trap is just large enough for a Medium-sized character’s leg. When a creature steps in the square, the trap catches and holds the leg. The target takes damage if it tries to move from that square or if a Disable Device attempt to free her fails by 5 or more.

Effect spikes (atk +10 melee, 2d4); spikes hold the target in place; the trap can be escaped with a DC 20 Disable Device check, DC 20 Escape Artist check, or DC 24 Strength check.

### [Arrow Trap](https://www.d20pfsrd.com/gamemastering/traps-hazards-and-special-terrains/traps/arrow-trap-cr-1) CR 1

Type mechanical; Perception DC 20; Disable Device DC 20

Trigger Touch; Reset None

Atk+15 (1d8+1/×3) First a single arrow

### [Bear Traps](https://www.d20pfsrd.com/gamemastering/traps-hazards-and-special-terrains/traps/bear-traps-cr-1) CR 1

Type mechanical; Perception DC 20; Disable Device DC 20

Trigger locaiton; Reset Manual

Atk+20 melee (2d6+3) Clamps onto a creature's ankle. Halves movement speed, or immobilises if attached to something solid. Creature can escape with a DC 20 Disable Device, DC 22 Escape Artist, or a DC 26 Strength check

### [Collapsing Floor](https://www.d20pfsrd.com/gamemastering/traps-hazards-and-special-terrains/traps/collapsing-floor-cr-1/) CR 1 

Type mechanical; Perception DC 20; Disable Device DC 25

Trigger location; Reset repair

Target falls down a 10ft pit for 1d6 damage. Affects all targets in a 10ft area. DC 15 Reflex save avoids

### [Spiked Snare](https://www.d20pfsrd.com/gamemastering/traps-hazards-and-special-terrains/traps/spiked-snare-cr-1/) CR 1

Type mechanical; Perception DC 20; Disable Device DC 15

Trigger touch; Reset manual

Effects CMB +10 (vs. target’s CMD; target gains grappled condition and is yanked 10 feet into the air); Atk +8 melee (2 spikes for 1d6+2 damage each)

### [Swinging Axe Trap](https://www.d20pfsrd.com/gamemastering/traps-hazards-and-special-terrains/traps/swinging-axe-trap-cr-1/) CR 1

Type mechanical; Perception DC 20; Disable Device DC 20

Trigger location; Reset manual

Effect Atk +10 melee (axe; 1d8+1/×3); multiple targets (all targets in a 10-ft. line)

### [Swinging Log Trap](https://www.d20pfsrd.com/gamemastering/traps-hazards-and-special-terrains/traps/swinging-log-trap-cr-1) CR 1

Type mechanical; Notice Perception DC 12; Disarm Disable Device DC 12

Trigger location; Reset manual; Associated Terrain forests or jungles; Effect Atk +5 melee (1d6+5/×2) and knocked prone; Reflex DC 15 negates prone condition

### [Spring Loaded Panel](https://www.d20pfsrd.com/gamemastering/traps-hazards-and-special-terrains/traps/spring-loaded-panel) CR 2

Type mechanical; Perception DC 22; Disable Device DC 22

Trigger touch; Reset manual

Effect panel flips up and pushes a victim 5ft in any direction (Reflex DC 15 avoids)

### [Falling Bricks](https://www.d20pfsrd.com/gamemastering/traps-hazards-and-special-terrains/traps/falling-bricks-cr-3/) CR 3 

Trigger manual; Reset manual

Effect falling bricks (all in a 10-ft.-square area); 4d6 damage (DC 20 Reflex save for half damage)
