from datetime import datetime
from glob import glob
from pathlib import Path
import os
from typing import List

from jinja2 import Environment, FileSystemLoader
import markdown2
from subprocess import call
import argparse

from dateutil import parser as dateParser

# Config
src_dir = Path("src")
template_dir = src_dir / "templates"
article_dir = src_dir / "articles"
index_page = "index.html"
output_dir = Path("output")
article_output = output_dir / "articles"

renderer = Environment(
    loader=FileSystemLoader("src/templates")
)

markdown = markdown2.Markdown(extras=["metadata", "tables", "footnotes"])


class Article:
    def __init__(self, name: str, content: str, date: datetime, outline: str):
        self.name = name.replace(".md", "")
        self.content = content
        assert isinstance(date, datetime), f"date must be a datetime not a {type(date)}. From {name}"
        self.date = date
        self.outline = outline


def render_template(name: str, **kwargs):
    print(f"Rendering {name}")

    template = renderer.get_template(name)
    with open(output_dir / name, 'w') as out_file:
        out_file.write(template.render(**kwargs))


def get_article(name: str) -> Article:
    with open(article_dir / name) as source:
        content = markdown.convert(source.read())
        metadata = content.metadata

        date = dateParser.parse(metadata.get("date"))

        return Article(name, content, date, metadata.get("outline"))


def render_article(article: Article, dev=False):
    print(f"Rendering {article.name}\t{article.date}")

    template = renderer.get_template("article.html")
    with open(output_dir / "articles" / article.name, 'w') as out_file:
        output = template.render(article=article, dev=dev)
        out_file.write(output)


def get_article_list() -> List[Article]:
    return [
        get_article(Path(file).name)
        for file in glob("src/articles/*.md")
    ]


def render_article_list():
    articles = sorted(
        get_article_list(),
        key=lambda article: article.date,
        reverse=True)
    return render_template("article_list.html", article_list=articles)


def render_sass():
    print("rendering sass")
    call(["sassc", src_dir / "sass/style.sass", output_dir / "style.css"])


def create_dirs():
    for path in [output_dir, article_output]:
        if path.exists() is False:
            os.mkdir(path)


def copy_files():
    call(["cp", "-r", "src/files", output_dir])
    print("Copied files")


argparser = argparse.ArgumentParser()
argparser.add_argument("--output")
argparser.add_argument("--dev", type=bool)

if __name__ == "__main__":
    args = argparser.parse_args()
    print(args)
    if args.output:
        output_dir = Path(args.output)
        article_output = output_dir / "articles"

    create_dirs()

    render_template("index.html", dev=args.dev)
    render_template("contact.html", dev=args.dev)
    render_template("projects.html", dev=args.dev)
    render_article_list()
    render_sass()
    copy_files()

    for article in get_article_list():
        render_article(article, dev=args.dev)
